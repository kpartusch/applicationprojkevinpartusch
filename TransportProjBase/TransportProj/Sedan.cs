﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        protected virtual int MaxExecutionCount { get { return 1; } }

        public Sedan(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
        }

        public override void Move(Direction direction, int maxSpaces)
        {
            switch (direction)
            {
                case Direction.Left:
                    MoveLeft(maxSpaces, 0);
                    break;
                case Direction.Right:
                    MoveRight(maxSpaces, 0);
                    break;
                case Direction.Down:
                    MoveDown(maxSpaces, 0);
                    break;
                case Direction.Up:
                    MoveUp(maxSpaces, 0);
                    break;
                default:
                    throw new ApplicationException(string.Format("Direction not valid, {0}", direction));
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Sedan moved to x - {0} y - {1}", XPos, YPos));
        }

        private void MoveUp(int maxSpaces, int executionCounter)
        {
            if (YPos < City.YMax && executionCounter < MaxExecutionCount && executionCounter < maxSpaces)
            {
                MoveUp(maxSpaces, executionCounter + 1);
                YPos++;
                WritePositionToConsole();
            }
        }

        private void MoveDown(int maxSpaces, int executionCounter)
        {
            if (YPos > 0 && executionCounter < MaxExecutionCount && executionCounter < maxSpaces)
            {
                MoveDown(maxSpaces, executionCounter + 1);
                YPos--;
                WritePositionToConsole();
            }
        }

        private void MoveRight(int maxSpaces, int executionCounter)
        {
            if (XPos < City.XMax && executionCounter < MaxExecutionCount && executionCounter < maxSpaces)
            {
                MoveRight(maxSpaces, executionCounter + 1);
                XPos++;
                WritePositionToConsole();
            }
        }

        private void MoveLeft(int maxSpaces, int executionCounter)
        {
            if (XPos > 0 && executionCounter < MaxExecutionCount && executionCounter < maxSpaces)
            {
                MoveLeft(maxSpaces, executionCounter + 1);
                XPos--;
                WritePositionToConsole();
            }
        }
    }
}
