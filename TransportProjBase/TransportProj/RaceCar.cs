using System;

namespace TransportProj
{
    public class RaceCar : Sedan
    {
        protected override int MaxExecutionCount { get { return 2; } }

        public RaceCar(int xPos, int yPos, City city, Passenger passenger)
            : base(xPos, yPos, city, passenger)
        {
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("RaceCar moved to x - {0} y - {1}", XPos, YPos));
        }
    }
}