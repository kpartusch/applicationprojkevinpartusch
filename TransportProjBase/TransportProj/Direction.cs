﻿namespace TransportProj
{
    public enum Direction
    {
        Left,
        Right,
        Up,
        Down
    }
}