using System;

namespace TransportProj
{
    public class CarController
    {
        public void DetermineNextAction(Car car, Passenger passenger, Action<Passenger> passengerFunc, int desiredXPosition, int desiredYPosition)
        {
            if (car.XPos != desiredXPosition)
            {
                if (car.XPos > desiredXPosition)
                {
                    car.Move(Direction.Left, car.XPos - desiredXPosition);
                }
                else
                {
                    car.Move(Direction.Right, desiredXPosition - car.XPos);
                }
            }

            else if (car.YPos != desiredYPosition)
            {
                if (car.YPos > desiredYPosition)
                {
                    car.Move(Direction.Down, car.YPos - desiredYPosition);
                }
                else
                {
                    car.Move(Direction.Up, desiredYPosition - car.YPos);
                }
            }

            else if (car.XPos == desiredXPosition && car.YPos == desiredYPosition)
            {
                passengerFunc.Invoke(passenger);
            }
        }
    }
}