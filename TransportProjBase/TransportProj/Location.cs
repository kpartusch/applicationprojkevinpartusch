﻿namespace TransportProj
{
    public class Location
    {
        public int XPos { get; private set; }
        public int YPos { get; private set; }

        public Location(int xPos, int yPos)
        {
            XPos = xPos;
            YPos = yPos;
        }
    }
}